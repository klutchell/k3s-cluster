# mosquitto

## deploy

```bash
# export kubeconfig
export KUBECONFIG="$(pwd)/kubeconfig"

# create namespace and set as current context
kubectl create namespace mosquitto

# create nfs persistent volume claim
kubectl apply -f mosquitto/claim.yaml

# apply configuration
kubectl apply -f mosquitto/configmap.yaml
kubectl apply -f mosquitto/deployment.yaml
kubectl apply -f mosquitto/service.yaml

# verify previous steps
kubectl get all -n mosquitto
kubectl describe pods -n mosquitto
kubectl logs -n mosquitto -c mosquitto -l app=mosquitto
```

## usage

```bash
# scale deployment as needed
kubectl scale --replicas=0 deploy/mosquitto -n mosquitto
kubectl scale --replicas=1 deploy/mosquitto -n mosquitto

# connect to running containers
kubectl -n mosquitto exec -it deploy/mosquitto -- /bin/bash
```

## references

- <https://github.com/hyperbolic2346/kubernetes/tree/master/mosquitto>
- <https://github.com/cloudnesil/eclipse-mosquitto-mqtt-broker-helm-chart>