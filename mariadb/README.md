# mariadb

## deploy

```bash
# export kubeconfig
export KUBECONFIG="$(pwd)/kubeconfig"

# create namespace and set as current context
kubectl create namespace mariadb

# create nfs persistent volume claim
kubectl apply -f mariadb/claim.yaml

# create secret file for credentials
kubectl create secret generic mariadb -n mariadb \
--from-literal='MYSQL_ROOT_PASSWORD=topsecret' \
--dry-run=client -o yaml > mariadb/secret.yaml

# apply configuration
kubectl apply -f mariadb/secret.yaml
kubectl apply -f mariadb/configmap.yaml
kubectl apply -f mariadb/deployment.yaml
kubectl apply -f mariadb/service.yaml
kubectl apply -f mariadb/cronjob.yaml

# verify previous steps
kubectl get all -n mariadb
kubectl describe pods -n mariadb
kubectl logs -n mariadb -l app=mariadb -c mariadb
kubectl logs -n mariadb -l app=mariadb -c cron
```

## usage

```bash
# scale deployment as needed
kubectl -n mariadb scale --replicas=0 deploy/mariadb
kubectl -n mariadb scale --replicas=1 deploy/mariadb

# connect to running containers
kubectl -n mariadb exec -it deploy/mariadb -- /bin/bash

# list users after initialization
kubectl -n mariadb exec -it deploy/mariadb -- mysql -u root --password --execute="SELECT User FROM mysql.user;"

# restore database from mysqldump file
kubectl -n mariadb exec -it deploy/mariadb -- mysql -u root --password < /var/lib/mysql/mysqldump-20.sql
kubectl -n mariadb exec -it deploy/mariadb -- mysql -u root --password --execute="FLUSH PRIVILEGES;"
```

## references

- <https://github.com/helm/charts/tree/master/stable/mariadb>
- <https://github.com/bitnami/charts/tree/master/bitnami/mariadb>
