# bookstack

## deploy

```bash
# export kubeconfig
export KUBECONFIG="$(pwd)/kubeconfig"

# create namespace and set as current context
kubectl create namespace bookstack

# create nfs persistent volume claim
kubectl apply -f bookstack/claim.yaml

# create secret file for credentials
kubectl create secret generic bookstack -n bookstack \
--from-literal='DB_USER=bookstack' \
--from-literal='DB_PASS=supersecret' \
--dry-run=client -o yaml > bookstack/secret.yaml

# apply configuration
kubectl apply -f bookstack/secret.yaml
kubectl apply -f bookstack/deployment.yaml
kubectl apply -f bookstack/service.yaml
kubectl apply -f bookstack/ingress.yaml

# verify previous steps
kubectl get all -n bookstack
kubectl describe pods -n bookstack
kubectl logs -n bookstack -c bookstack -l app=bookstack
```

## usage

```bash
# create mysql user in remote database (mariadb)
kubectl -n mariadb exec -it deploy/mariadb -- mysql -u root --password --execute="CREATE DATABASE bookstack;"
kubectl -n mariadb exec -it deploy/mariadb -- mysql -u root --password --execute="CREATE USER 'bookstack'@'%' IDENTIFIED BY 'supersecret';"
kubectl -n mariadb exec -it deploy/mariadb -- mysql -u root --password --execute="GRANT ALL ON bookstack.* TO 'bookstack'@'%';FLUSH PRIVILEGES;"

# scale deployment as needed
kubectl scale --replicas=0 deploy/bookstack -n bookstack
kubectl scale --replicas=1 deploy/bookstack -n bookstack

# connect to running containers
kubectl -n bookstack exec -it deploy/bookstack -- /bin/bash
```

## references

- <https://github.com/BookStackApp/devops/blob/master/scripts/installation-ubuntu-20.04.sh>
- <https://www.digitalocean.com/community/tutorials/how-to-create-a-new-user-and-grant-permissions-in-mysql>
- <https://github.com/linuxserver/docker-bookstack>