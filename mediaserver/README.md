# mediaserver

## deploy

```bash
# export kubeconfig
export KUBECONFIG="$(pwd)/kubeconfig"

# create namespace and set as current context
kubectl create namespace mediaserver

# apply configuration
export $(cat .env | xargs)
envsubst < mediaserver/nzbhydra.yaml | kubectl apply -f -

# verify previous steps
kubectl get endpoints,service,ingress -n mediaserver
kubectl get certificaterequest,certificate -o wide -n mediaserver
```
