# rook-ceph

- <https://rook.io/docs/rook/v1.4/ceph-quickstart.html>
- <https://github.com/rook/rook/tree/release-1.4/cluster/examples/kubernetes/ceph>

## deploy

```bash
# export kubeconfig
export KUBECONFIG="$(pwd)/kubeconfig"

kubectl apply -f rook-ceph/common.yaml
kubectl apply -f rook-ceph/operator.yaml
kubectl apply -f rook-ceph/cluster.yaml

watch kubectl get pods -n rook-ceph

# install toolbox
kubectl apply -f rook-ceph/toolbox.yaml
kubectl -n rook-ceph get pod -l "app=rook-ceph-tools"
kubectl -n rook-ceph exec -it $(kubectl -n rook-ceph get pod -l "app=rook-ceph-tools" -o jsonpath='{.items[0].metadata.name}') -- ceph status

# install dashboard
kubectl apply -f rook-ceph/dashboard-external-https.yaml
kubectl -n rook-ceph get service
kubectl -n rook-ceph get secret rook-ceph-dashboard-password -o jsonpath="{['data']['password']}" | base64 --decode && echo

# create a secure channel to access the cluster
kubectl proxy
```

<http://localhost:8001/api/v1/namespaces/rook-ceph/services/https:rook-ceph-mgr-dashboard:https-dashboard/proxy/>
