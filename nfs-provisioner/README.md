# nfs-provisioner

## deploy

```bash
export KUBECONFIG="$(pwd)/kubeconfig"

# create the namespace
kubectl create namespace nfs-provisioner

# deploy the provisioner
kubectl apply -f nfs-provisioner/deployment.yaml
kubectl apply -f nfs-provisioner/rbac.yaml
kubectl apply -f nfs-provisioner/class.yaml

# check pods are running
kubectl get all -n nfs-provisioner

# test the provisioner
kubectl apply -f nfs-provisioner/claim.yaml
kubectl get pvc,pv
kubectl delete -f nfs-provisioner/claim.yaml
```
## usage

```bash
# start a shell session in the nfs-provisioner
kubectl -n nfs-provisioner exec -it deploy/nfs-provisioner -- /bin/bash
```