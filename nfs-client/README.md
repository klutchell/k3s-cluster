# nfs-client

## deploy

```bash
# export kubeconfig
export KUBECONFIG="$(pwd)/kubeconfig"

# deploy the provisioner
# make sure user:nobody has write access to share
kubectl create namespace nfs-client
kubectl apply -f nfs-client/rbac.yaml
kubectl apply -f nfs-client/deployment-arm.yaml
kubectl apply -f nfs-client/class.yaml

# test the provisioner
kubectl apply -f nfs-client/test-claim.yaml
kubectl get pvc,pv
kubectl delete -f nfs-client/test-claim.yaml
```
