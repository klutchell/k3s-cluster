# local-path-storage

- <https://github.com/rancher/local-path-provisioner>
- <https://medium.com/swlh/learn-kubernetes-external-storage-provisioner-by-looking-at-how-local-path-provisioner-18ff618a565b>

## deploy

```bash
# export kubeconfig
export KUBECONFIG="$(pwd)/kubeconfig"

kubectl apply -f https://raw.githubusercontent.com/rancher/local-path-provisioner/master/deploy/local-path-storage.yaml

kubectl -n local-path-storage get pod

kubectl -n local-path-storage logs -f -l app=local-path-provisioner
```

## test

```bash
# create a persistent volume claim and test pod
kubectl create -f https://raw.githubusercontent.com/rancher/local-path-provisioner/master/examples/pvc.yaml
kubectl create -f https://raw.githubusercontent.com/rancher/local-path-provisioner/master/examples/pod.yaml

# verify pvc and pod are created and bound
kubectl get pv
kubectl get pvc
kubectl get pod -o wide

# write test file to volume
kubectl exec volume-test -- sh -c "echo local-path-test > /data/test"

# delete the pod
kubectl delete -f https://raw.githubusercontent.com/rancher/local-path-provisioner/master/examples/pod.yaml

# confirm the pod is gone, then recreate it
kubectl create -f https://raw.githubusercontent.com/rancher/local-path-provisioner/master/examples/pod.yaml

# verify if the test file is still available
kubectl exec volume-test -- cat /data/test

# delete the pod and the pvc
kubectl delete -f https://raw.githubusercontent.com/rancher/local-path-provisioner/master/examples/pod.yaml
kubectl delete -f https://raw.githubusercontent.com/rancher/local-path-provisioner/master/examples/pvc.yaml
```
