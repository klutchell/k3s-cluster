# boostrap with k3os

- <https://github.com/rancher/k3os>
- <https://github.com/rancher/k3os#configuration-reference>
- <https://github.com/sgielen/picl-k3os-image-generator>

## copy kubeconfig

```bash
scp rancher@192.168.8.4:/etc/rancher/k3s/k3s.yaml ./kubeconfig
sed 's|127\.0\.0\.1|192.168.8.4|' -i ./kubeconfig
export KUBECONFIG="$(pwd)/kubeconfig"
```
