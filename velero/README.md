# velero

- <https://velero.io/docs/v1.4/basic-install/>
- <https://velero.io/docs/v1.4/restic/>
- <https://github.com/StackPointCloud/ark-plugin-digitalocean>

## deploy

```bash
# copy kubeconfig
cp kubeconfig ~/.kube/config

cd velero

# download
wget https://github.com/vmware-tanzu/velero/releases/download/v1.4.2/velero-v1.4.2-linux-amd64.tar.gz
tar xvf velero-v1.4.2-linux-amd64.tar.gz --strip-components 1 velero
sudo install velero /usr/local/bin/
rm velero-v1.4.2-linux-amd64.tar.gz

# install
velero install \
    --use-restic \
    --provider velero.io/aws \
    --bucket klutchell \
    --plugins velero/velero-plugin-for-aws:v1.0.0,digitalocean/velero-plugin:v1.0.0 \
    --backup-location-config s3Url=https://nyc3.digitaloceanspaces.com,region=nyc3 \
    --use-volume-snapshots=false \
    --secret-file=./cloud-credentials

export KUBE_EDITOR='code --wait'
kubectl -n velero edit deploy/velero
```

## uninstall

- <https://velero.io/docs/main/uninstalling/>

```bash
kubectl delete namespace/velero clusterrolebinding/velero
kubectl delete crds -l component=velero
```
