# cert-manager

## deploy

```bash
# export kubeconfig
export KUBECONFIG="$(pwd)/kubeconfig"

# source local environment variables
export $(cat .env | xargs)

kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v0.15.2/cert-manager.yaml
envsubst < cert-manager/clusterissuer.yaml | kubectl apply -f -
```

## references

- <https://github.com/jetstack/cert-manager/releases>
- <https://cert-manager.io/docs/installation/kubernetes/>
- <https://pascalw.me/blog/2019/07/02/k3s-https-letsencrypt.html>
