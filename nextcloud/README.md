# nextcloud

## deploy

```bash
# export kubeconfig
export KUBECONFIG="$(pwd)/kubeconfig"

# create namespace and set as current context
kubectl create namespace nextcloud

# create nfs persistent volume claim
kubectl apply -f nextcloud/claim.yaml

# create secret file for credentials
kubectl create secret generic nextcloud -n nextcloud \
--from-literal='MYSQL_USER=nextcloud' \
--from-literal='MYSQL_PASSWORD=supersecret' \
--from-literal='NEXTCLOUD_ADMIN_USER=secret' \
--from-literal='NEXTCLOUD_ADMIN_PASSWORD=supersecret' \
--dry-run=client -o yaml > nextcloud/secret.yaml

# apply configuration
export $(cat .env | xargs)
kubectl apply -f nextcloud/secret.yaml
kubectl apply -f nextcloud/configmap.yaml
envsubst < nextcloud/deployment.yaml | kubectl apply -f -
envsubst < nextcloud/service.yaml | kubectl apply -f -
envsubst < nextcloud/ingress.yaml | kubectl apply -f -

# verify previous steps
kubectl get all -n nextcloud
kubectl describe pods -n nextcloud
kubectl logs -n nextcloud -l app=nextcloud -c nextcloud
kubectl logs -n nextcloud -l app=nextcloud -c nginx
kubectl logs -n nextcloud -l app=nextcloud -c cron
kubectl get certificaterequest,certificate -o wide -n nextcloud
```

## usage

```bash
# create mysql user in remote database (mariadb)
kubectl -n mariadb exec -it deploy/mariadb -- mysql -u root --password --execute="CREATE DATABASE nextcloud;"
kubectl -n mariadb exec -it deploy/mariadb -- mysql -u root --password --execute="CREATE USER 'nextcloud'@'%' IDENTIFIED BY 'supersecret';"
kubectl -n mariadb exec -it deploy/mariadb -- mysql -u root --password --execute="GRANT ALL ON nextcloud.* TO 'nextcloud'@'%';FLUSH PRIVILEGES;"

# scale deployment as needed
kubectl scale --replicas=0 deploy/nextcloud -n nextcloud
kubectl scale --replicas=1 deploy/nextcloud -n nextcloud

# connect to running containers
kubectl -n nextcloud exec -it deploy/nextcloud -c nextcloud -- /bin/sh
kubectl -n nextcloud exec -it deploy/nextcloud -c nginx -- /bin/sh

# fix nextcloud database warnings
kubectl -n nextcloud exec -it deploy/nextcloud -- /bin/sh -c '
apk add --no-cache sudo
sudo -u www-data php /var/www/html/occ maintenance:mode --on
sudo -u www-data php /var/www/html/occ db:add-missing-indices
sudo -u www-data php /var/www/html/occ db:convert-filecache-bigint --no-interaction
sudo -u www-data php /var/www/html/occ maintenance:mode --off
'

# set trusted domains
kubectl -n nextcloud exec -it deploy/nextcloud -c nextcloud -- /bin/sh -c '
apk add --no-cache sudo
sudo -u www-data php /var/www/html/occ config:system:set trusted_domains 0 --value="nextcloud.192.168.8.4.nip.io"
sudo -u www-data php /var/www/html/occ config:system:set trusted_domains 1 --value="nextcloud.example.com"
'

# set trusted proxies
kubectl -n nextcloud exec -it deploy/nextcloud -c nextcloud -- /bin/sh -c '
apk add --no-cache sudo
sudo -u www-data php /var/www/html/occ config:system:set trusted_proxies 0 --value="traefik"
'

# set overwriteprotocol to https
# this seems to be required for client token flow?
kubectl -n nextcloud exec -it deploy/nextcloud -c nextcloud -- /bin/sh -c '
apk add --no-cache sudo
sudo -u www-data php /var/www/html/occ config:system:set overwriteprotocol --value="https"
'

# set overwritehost
kubectl -n nextcloud exec -it deploy/nextcloud -c nextcloud -- /bin/sh -c '
apk add --no-cache sudo
sudo -u www-data php /var/www/html/occ config:system:set overwrite.cli.url --value="https://nextcloud.example.com/"
sudo -u www-data php /var/www/html/occ config:system:set overwritehost --value="nextcloud.example.com"
'
```

## develop

```bash
# generate helm template for reference
helm template nextcloud stable/nextcloud \
--set image.tag=18-fpm-alpine \
--set ingress.enabled=true \
--set nextcloud.host=nextcloud.example.com \
--set internalDatabase.enabled=false \
--set externalDatabase.enabled=true \
--set externalDatabase.type=mysql \
--set externalDatabase.host=mariadb.mariadb \
--set externalDatabase.database=nextcloud \
--set externalDatabase.user=nextcloud \
--set externalDatabase.password=secret \
--set nginx.enabled=true \
--set mariadb.enabled=false \
--set redis.enabled=false \
--set cronjob.enabled=true \
--set persistence.enabled=true \
--set persistence.existingClaim=config \
--namespace nextcloud > helm/template.yaml
```

## references

- <https://github.com/helm/charts/tree/master/stable/nextcloud>
- <https://kauri.io/deploy-nextcloud-on-kuberbetes:-the-self-hosted-dropbox/f958350b22794419b09fc34c7284b02e/a>
- <https://www.andremotz.com/nextcloud-docker-on-kubernetes-cluster-ssl-certificates/>
- <https://github.com/andremotz/nextcloud-kubernetes>
- <https://docs.traefik.io/v1.7/configuration/backends/kubernetes/>
