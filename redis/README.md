# redis

## deploy

```bash
# export kubeconfig
export KUBECONFIG="$(pwd)/kubeconfig"

# create namespace and set as current context
kubectl create namespace redis

# create nfs persistent volume claim
kubectl apply -f redis/claim.yaml

# apply configmap, deployment, and service
kubectl apply -f redis/configmap.yaml
kubectl apply -f redis/deployment.yaml
kubectl apply -f redis/service.yaml

# verify previous steps
kubectl -n redis get all
kubectl -n redis describe pods
kubectl -n redis logs -c redis -l app=redis
```

## usage

```bash
# scale deployment as needed
kubectl -n redis scale --replicas=0 deploy/redis
kubectl -n redis scale --replicas=1 deploy/redis

# connect to running containers
kubectl -n redis exec -it deploy/redis -- /bin/sh
```
