# homeassistant

## deploy

```bash
# export kubeconfig
export KUBECONFIG="$(pwd)/kubeconfig"

# create namespace and set as current context
kubectl create namespace homeassistant

# create nfs persistent volume claim
kubectl apply -f homeassistant/claim.yaml

# apply deployment, service, and ingress
kubectl apply -f homeassistant/deployment.yaml
kubectl apply -f homeassistant/service.yaml
kubectl apply -f homeassistant/ingress.yaml

# verify previous steps
kubectl get all -n homeassistant
kubectl describe pods -n homeassistant
kubectl logs -n homeassistant -l app=homeassistant -c homeassistant
kubectl logs -n homeassistant -l app=homeassistant -c vscode
```

## usage

```bash
# scale deployment as needed
kubectl scale --replicas=0 deploy/homeassistant -n homeassistant
kubectl scale --replicas=1 deploy/homeassistant -n homeassistant

# connect to running containers
kubectl exec -it deploy/homeassistant -n homeassistant -- /bin/bash
```

## develop

```bash
# generate helm template for reference
helm template stable/home-assistant \
--set image.tag=stable \
--set ingress.enabled=true \
--set ingress.hosts[0]=homeassistant.192.168.8.4.nip.io \
--set configurator.enabled=true \
--set configurator.ingress.enabled=true \
--set configurator.image.tag=0.4.0-arm \
--set configurator.ingress.hosts[0]=configurator.192.168.8.4.nip.io \
--set vscode.enabled=true \
--set vscode.image.tag=3.4.0 \
--set vscode.ingress.enabled=true \
--set vscode.ingress.hosts[0]=vscode.192.168.8.4.nip.io \
--set persistence.enabled=true \
--set persistence.existingClaim=config \
--namespace homeassistant > template.yaml
```

## references

- <https://github.com/hyperbolic2346/kubernetes/tree/master/homeassistant>
- <https://github.com/helm/charts/tree/master/stable/home-assistant>
