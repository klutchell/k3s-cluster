# k3s-cluster

## boostrap

[boostrap with ansible](./ansible/README.md)

[boostrap with k3os](./k3os/README.md)

## install kubectl

- <https://kubernetes.io/docs/tasks/tools/install-kubectl/>

```bash
# install kubectl on local workstation
curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
sudo install ./kubectl /usr/local/bin/kubectl
```

## install helm

- <https://helm.sh/docs/intro/install/>

```bash
# install helm 3.x on local workstation
curl -fsSL https://get.helm.sh/helm-v3.2.1-linux-amd64.tar.gz -o - | tar -xzf -
sudo install linux-amd64/helm /usr/local/bin/helm
rm -rf linux-amd64

# add helm stable repo
helm repo add stable https://kubernetes-charts.storage.googleapis.com
helm repo update
```
